
        let box = window.innerWidth / 4;

        //function to change body backgroud color
        function bgColor(color) {
            document.body.style.backgroundColor = color;
        }

        window.onmousemove = function(cord) {
            let x = cord.clientX;
            if (x < box) {
                bgColor("red");
            } else if (x < box * 2) {
                bgColor("orange");
            } else if (x < box * 3) {
                bgColor("yellow");
            } else if (x < box * 4) {
                bgColor("magenta");
            }
        }